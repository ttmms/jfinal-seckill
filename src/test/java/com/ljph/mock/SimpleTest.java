package com.ljph.mock;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Created by yuzhou on 16/8/1.
 */
public class SimpleTest {

    private static final Logger _log = LoggerFactory.getLogger(SimpleTest.class);

    @Test
    public void testMockList(){

        _log.info("{}", DigestUtils.md5Hex("dfdfdf"));

        _log.info("Mock a list to test");
        List<String> list = mock(List.class);
        when(list.get(0)).thenReturn("helloworld");
        String result = list.get(0);

        when(list.size()).thenReturn(1);
        System.out.print(list.size());

        verify(list).get(0);
        assertEquals("helloworld", result);
    }
}
